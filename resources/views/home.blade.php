<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"/>


    <!-- CSS -->
    <link href="{{ asset('style.css') }}?{{ \Carbon\Carbon::now()->timestamp }}" rel="stylesheet" type="text/css"/>

    <!-- favicon icon-->
    <link rel="icon" type="image/x-icon" href="ASSET/favicon/favicon_package_v0.16/android-chrome-192x192.png"/>

    <title>{{ strtoupper($brand) }} > HARSLOTNAS di Situs Judi Slot Online Terpercaya dan Judi Online Terbaik</title>
    <meta name="description" content="HARSLOTNAS merupakan event yang diselanggarakan oleh situs judi slot online {{ strtoupper($brand) }} dengan total hadiah sebesar 1,2 MILIYAR RUPIAH, apa saja isi hadiah tersebut?">
    <meta name="canonical" content="https://www.nebraskahistory.org">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-190816597-15"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-190816597-15');
    </script>
</head>
<body>
<div id="particles-js"></div>
<!--  navbar start -->
<nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top" style="background-color: rgb(184, 186, 186)">
    <div class="container">
        <a class="navbar-brand" href="/">HARSLOTNAS</a>
        <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ms-auto" style="font-size: 20px">
                <li class="nav-item">
                    <a class="nav-link active text-center" aria-current="page" href="https://34.117.9.220/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active text-center" aria-current="page" href="#section2">Terms & Conditions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active text-center" aria-current="page" href="#section3">Grand Prize</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active text-center" aria-current="page" href="#section4">Leaderboards</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--  navbar end -->
<!-- header-->

<header id="header" style="margin-top: 58px">
    <div class="header-harslotnas">
        <div class="row g-0">
            <div class="col-12">
                <img src="ASSET/IMG/HEAD BANNER BANNER HARSLOTNAS_DRAGON copy 3.webp" alt="banner harslotnas" class="img-fluid w-100" />
            </div>
        </div>
    </div>
</header>

<section id="section4" class="pt-5">
    <div class="leaderboard justify-content-center">
        <div class="row justify-content-center">
            <div class="col-10 d-flex flex-column">
                <video autoplay muted playsinline class="d-block d-sm-none">
                    <source src="{{ asset('ASSET/hsn_LEADERBOARD FHD PORTRAIT.m4v') }}" type="video/mp4">
                    Your browser does not support HTML video.
                </video>
                <video autoplay muted playsinline class="d-none d-md-block">
                    <source src="{{ asset('ASSET/hsn_LEADERBOARD FHD LANDSCAPE.m4v') }}" type="video/mp4">
                    Your browser does not support HTML video.
                </video>
            </div>
        </div>
    </div>
</section>

<!-- section 1 -->
<section id="section1" class="harslotnas d-none">
    <div class="harslotnas">
        <div class="row justify-content-center mt-3 mb-3">
            <div class="col-12 d-flex">
                <img src="ASSET/IMG/ASSET LANDING PAGE-05 (1).webp" class="img-fluid mx-auto" alt="logo-harslotnas"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-8 d-flex">
                <img src="ASSET/IMG/ASSET LANDING PAGE-06 (1).webp" class="img-fluid mx-auto" alt="logo-periode"/>
            </div>
        </div>
        <!-- tombol join start-->
        <div class="row justify-content-center mt-4">
            <div class="col-8">
                <a href="{{ $url }}" class="w-100 d-flex"> <img src="ASSET/IMG/btn-{{ $brand }}.webp" class="img-fluid mx-auto btn-daftar"
                                                  alt="tombol daftar {{ $brand }}" title="tombol daftar {{ $brand }}"/></a>
            </div>
        </div>
        <!-- tombol join end-->
    </div>
</section>
<!-- section 1 end-->
<!-- section 2 -->
<section id="section2" class="pt-5 d-none">
    <div class="termandcondition mx-auto">
        <div class="row justify-content-center mb-4 mt-5">
            <div class="col-8">
                <img src="ASSET/IMG/ASSET LANDING PAGE-14 (1).webp" class="logoterm img-fluid mx-auto" alt="termandcondition"/>
            </div>
        </div>
        <!-- rules start -->
        <div class="rules1">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/1.webp" class="number1 img-fluid" alt="number1"/>
                </div>
                <div class="col-8">
                    <p class="numberone text-start"><b>EVENT INI AKAN BERLANGSUNG DI STARS77, LUCK99, 77LUCKS, 77DRAGON
                            DAN JOKER99.</b></p>
                </div>
            </div>
        </div>
        <div class="rules2">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/2.webp" class="img-fluid h-auto" alt="number2"/>
                </div>
                <div class="col-8">
                    <p class=" numberone text-start"><b>AKAN DILAKUKAN BERDASARKAN JUMLAH TURNOVER TERBESAR DARI PERMAINAN MEMBER
                            SELAMA PERIODE EVENT.</b></p>
                </div>
            </div>
        </div>
        <div class="rules3">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/3.webp" class="img-fluid h-auto" alt="number3"/>
                </div>
                <div class="col-8">
                    <p class=" numberone text-start"><b>HASIL TURNOVER MEMBER DARI WEBSITE STARS77, LUCK99, 77LUCKS, 77DRAGON DAN
                            JOKER99 AKAN DIMASUKAN KEDALAM 1(SATU) LEADERBOARD.</b></p>
                </div>
            </div>
        </div>
        <div class="rules4">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/4.webp" class="img-fluid h-auto" alt="number4"/>
                </div>
                <div class="col-8">
                    <p class="numberone text-start"><b>SETIAP HARINYA UPDATE LEADERBOARD MEMBER DAPAT DILIHAT DI HALAMAN WEBSITE INI.</b></p>
                </div>
            </div>
        </div>
        <div class="rules5">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/5.webp" class="img-fluid h-auto" alt="number5"/>
                </div>
                <div class="col-8">
                    <p class="numberone text-start"><b>PENGUMUMAN PEMENANG AKAN DILAKUKAN PADA 1 JANUARI 2021 PKL 00.00 WIB.</b>
                    </p>
                </div>
            </div>
        </div>
        <div class="rules6">
            <div class="row pb-3 justify-content-center">
                <div class="col-2 col-sm-1">
                    <img src="ASSET/IMG/6.webp" class="img-fluid h-auto" alt="number6"/>
                </div>
                <div class="col-8">
                    <p class=" numberone text-start">
                        <b>KLAIM HADIAH GRANDPRIZE DAPAT DILAKUKAN OLEH PEMENANG SELAMBAT LAMBATNYA 2X24JAM SETELAH
                            PENGUMUMAN, APABILA PEMENANG TIDAK MELAKUKAN KLAIM LEWAT DARI WAKTU YANG TELAH DITENTUKAN
                            MAKA HADIAH AKAN DIANGGAP HANGUS.</b>
                    </p>
                </div>
            </div>
        </div>
        <!-- rules and -->
    </div>
</section>
<!-- section 2 end-->
<!-- section 3 -->
<section id="section3" class="pt-5">
    <div class="grandprize">
        <div class="row justify-content-center mt-3 mb-2">
            <div class="col-8">
                <img src="ASSET/IMG/ASSET LANDING PAGE-15 (1).webp" class="img-fluid" alt="logo-GRANDPRIZE"/>
            </div>
        </div>
        <div class="row justify-content-center mt-2">
            <div class="col-8">
                <img src="ASSET/IMG/ASSET LANDING PAGE-22.webp" class="img-fluid" alt="logo-giftvehicle"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-8">
                <p class="Textnotif2"><b>1 Unit Toyota Fortuner, 1 Unit Honda Brio RS, 1 Unit Honda SCOOPY, 2 Unit
                        Laptop Gaming ROG , 5 Unit ROG PHONE.</b></p>
            </div>
        </div>
        <div class="row justify-content-center mb-4 mt-3">
            <div class="col-10">
                <img src="ASSET/IMG/ASSET LANDING PAGE-24.webp" class="img-fluid" alt="logo-notifgift02"/>
            </div>
        </div>
    </div>
</section>
<!-- section 3 end -->
<!-- section 4 -->
<section id="section4" class="pt-5 d-none">
    <div class="leaderboard justify-content-center d-block d-sm-none">
        <div class="row justify-content-center">
            <div class="col-10 d-flex flex-column">
                <div class="lb-wrapper">
                <img src="ASSET/IMG/bg-leaderboard-mobile-no-date.webp" class="leaderboard img-fluid m-auto"
                     alt="logo LEADERBOARD"/>
                    <div class="date">{{ \Carbon\Carbon::now()->subDay(1)->toFormattedDateString() }}</div>
                    <div class="first-wrapper">
                        {{--<div class="row">
                            <div class="col-12">
                                <div class="card mb-3 bg-transparent">
                                    <div class="row g-0">
                                        <div class="col-md-2 offset-2 col-2 d-flex justify-content-end">
                                            <img src="{{ asset('ASSET/IMG/lb-1.png') }}" class="img-fluid" alt="...">
                                        </div>
                                        <div class="col-md-6 col-6">
                                            <div class="card-body">
                                                <h6 class="card-title">Jajangmyeon</h6>
                                                <p class="card-text"><small>Rp. 80.**</small></p>
                                                <img src="{{ asset('ASSET/IMG/stars77-logo.png') }}" class="img-fluid w-50">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        <div class="second-wrapper d-flex justify-content-center">
                            <div class="lb-item w-100 d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-1-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="card-title">{{ $leaderboard[0]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[0]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[0]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>

                        </div>
                        <div class="second-wrapper d-flex mt-3">
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-2-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[1]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[1]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[1]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-3-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[2]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[2]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[2]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-4-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller ">{{ $leaderboard[3]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[3]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[3]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-5-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[4]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[4]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[4]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-6-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[5]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[5]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[5]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-7-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[6]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[6]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[6]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-8-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[7]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[7]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[7]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-9-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[8]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[8]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[8]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex justify-content-center mt-1 ">
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-10-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[9]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[9]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[9]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <p class="text-center"><small>Jumlah TURNOVER dalam ribuan .000</small></p>
            </div>
        </div>
        <!-- sponsored start -->

        <div class="row justify-content-center mx-auto">
            <div class="col-10 d-flex">
                <img src="ASSET/IMG/btn-join-sekarang+-+Salin.webp" class="img-fluid m-auto" alt="btn-jointelegram"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-3">
                <img src="ASSET/IMG/ASSET LANDING PAGE-07 (1).webp" class="img-fluid" alt="logo-sportedby"/>
            </div>
        </div>
        <div class="row justify-content-center pb-4">
            <div class="col-8">
                <img src="ASSET/IMG/ASSET LANDING PAGE-08 (1).webp" class="img-fluid" alt="logo-sportedbyver.2"/>
            </div>
        </div>
        <!-- sponsored and -->
    </div>
    <div class="leaderboard justify-content-center d-none d-md-block">
        <div class="row justify-content-center">
            <div class="col-10 d-flex flex-column">
                <div class="lb-wrapper">
                    <img src="ASSET/IMG/bg-leaderboard-desktop-no-date.webp" class="leaderboard img-fluid m-auto"
                         alt="logo LEADERBOARD"/>
                    <div class="date">{{ \Carbon\Carbon::now()->subDay(1)->toFormattedDateString() }}</div>
                    <div class="first-wrapper">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="lb-item w-100 d-flex">
                                    <img src="{{ asset('ASSET/IMG/lb-1-square.png') }}" class="img-fluid number">
                                    <div class="lb-name">
                                        <h6 class="card-title">{{ $leaderboard[0]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[0]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[0]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="lb-item d-flex">
                                    <img src="{{ asset('ASSET/IMG/lb-2-square.png') }}" class="img-fluid number">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[1]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[1]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[1]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="lb-item d-flex">
                                    <img src="{{ asset('ASSET/IMG/lb-3-square.png') }}" class="img-fluid number">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[2]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[2]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[2]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pt-5">
                            <div class="col-md-3"><div class="lb-item d d-flex w-40">
                                    <img src="{{ asset('ASSET/IMG/lb-4-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class=" ">{{ $leaderboard[3]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[3]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[3]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                                </div>
                            <div class="col-md-3"><div class="lb-item d d-flex w-40">
                                    <img src="{{ asset('ASSET/IMG/lb-5-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[4]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[4]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[4]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div></div>
                            <div class="col-md-3"><div class="lb-item d d-flex w-40" style="margin-right: inherit!important;
    margin-left: inherit!important;">
                                    <img src="{{ asset('ASSET/IMG/lb-6-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[5]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[5]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[5]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                                </div>
                            <div class="col-md-3">
                                <div class="lb-item d-flex w-40">
                                    <img src="{{ asset('ASSET/IMG/lb-7-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[6]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[6]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[6]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row pt-5">
                            <div class="col-md-3"><div class="lb-item d d-flex w-40">
                                    <img src="{{ asset('ASSET/IMG/lb-8-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[7]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[7]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[7]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div>
                                </div>
                            <div class="col-md-3"><div class="lb-item d d-flex w-40">
                                    <img src="{{ asset('ASSET/IMG/lb-9-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[8]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[8]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[8]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div></div>
                            <div class="col-md-3"><div class="lb-item d d-flex">
                                    <img src="{{ asset('ASSET/IMG/lb-10-square.png') }}" class="img-fluid number small">
                                    <div class="lb-name">
                                        <h6 class="">{{ $leaderboard[9]->player_id }}</h6>
                                        <p class="turn-over"><small>Rp. {{ $leaderboard[9]->turnover }}</small></p>
                                        <img src="{{ asset('ASSET/IMG/'. $leaderboard[9]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                    </div>
                                </div></div>
                        </div>
                        {{--<div class="row">
                            <div class="col-12">
                                <div class="card mb-3 bg-transparent">
                                    <div class="row g-0">
                                        <div class="col-md-2 offset-2 col-2 d-flex justify-content-end">
                                            <img src="{{ asset('ASSET/IMG/lb-1.png') }}" class="img-fluid" alt="...">
                                        </div>
                                        <div class="col-md-6 col-6">
                                            <div class="card-body">
                                                <h6 class="card-title">Jajangmyeon</h6>
                                                <p class="card-text"><small>Rp. 80.**</small></p>
                                                <img src="{{ asset('ASSET/IMG/stars77-logo.png') }}" class="img-fluid w-50">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                        {{--<div class="second-wrapper d-flex justify-content-center">
                            <div class="lb-item w-100 d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-1-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="card-title">{{ $leaderboard[0]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[0]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[0]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>

                        </div>
                        <div class="second-wrapper d-flex mt-3">
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-2-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[1]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[1]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[1]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-3-square.png') }}" class="img-fluid number">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[2]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[2]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[2]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-4-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller ">{{ $leaderboard[3]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[3]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[3]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-5-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[4]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[4]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[4]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-6-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[5]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[5]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[5]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-7-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[6]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[6]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[6]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex mt-1 justify-content-center">
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-8-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[7]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[7]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[7]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                            <div class="lb-item d-flex w-40">
                                <img src="{{ asset('ASSET/IMG/lb-9-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[8]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[8]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[8]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>
                        <div class="second-wrapper d-flex justify-content-center mt-1 ">
                            <div class="lb-item d-flex">
                                <img src="{{ asset('ASSET/IMG/lb-10-square.png') }}" class="img-fluid number small">
                                <div class="lb-name">
                                    <h6 class="smaller">{{ $leaderboard[9]->player_id }}</h6>
                                    <p class="turn-over"><small>Rp. {{ $leaderboard[9]->turnover }}</small></p>
                                    <img src="{{ asset('ASSET/IMG/'. $leaderboard[9]->bo_name .'-logo.png') }}" class="img-fluid w-50">
                                </div>
                            </div>
                        </div>--}}

                    </div>

                </div>
                <p class="text-center"><small>Jumlah TURNOVER dalam ribuan .000</small></p>
            </div>
        </div>
        <!-- sponsored start -->

        <div class="row justify-content-center mx-auto">
            <div class="col-10 d-flex"><a href="{{ $telegram }}" class="m-auto">
                <img src="ASSET/IMG/btn-join-sekarang+-+Salin.webp" class="img-fluid m-auto" alt="btn-jointelegram"/></a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-3">
                <img src="ASSET/IMG/ASSET LANDING PAGE-07 (1).webp" class="img-fluid" alt="logo-sportedby"/>
            </div>
        </div>
        <div class="row justify-content-center pb-4">
            <div class="col-8">
                <img src="ASSET/IMG/ASSET LANDING PAGE-08 (1).webp" class="img-fluid" alt="logo-sportedbyver.2"/>
            </div>
        </div>
        <!-- sponsored and -->
    </div>
</section>
<!-- section 4 end -->
<div class="seo-text p-3 container">
    <div class="row">
        <div class="col-lg-12">

            @if($brand == "stars77")
                <h1><strong>STARS77 - Situs Judi Slot Online Terlengkap &amp; Judi Online Terpercaya No.1 Di Indonesia</strong></h1>

                <p>Slot online merupakan salah satu jenis permainan yang dimana dimainkan dengan cara memutar mesin slot online dan dimenangkan ketika pemain mendapatkan kombinasi yang tepat. Di jaman sekarang ini sudah sangat mudah untuk memainkan slot online. Hanya dengan bermodalkan koneksi internet, para penjudi sudah bisa memainkan slot online kapanpun. Tidak perlu repot untuk datang ke tempat bermain judi secara langsung. STARS77 merupakan situs yang tepat untuk bermain slot online. Karena situs tersebut memiliki tingkat kemenangan tertinggi. Saat ini banyak judi online yang tersebar di Indonesia, atau yang sudah akrab sebagai salah satu metode cari uang tambahan. Dalam permainan game slot online, Anda akan memiliki berbagai macam permainan yang menarik serta memberikan keuntungan, jika Anda serius dalam memainkan game slot online tersebut. Apalagi ditambah dengan perubahan teknologi yang semakin maju, juga banyak perusahaan game slot online beradu untuk memberikan beberapa perjudian online dengan uang asli. Dengan didukung jaringan maupun akses internet yang lancar, Anda tentunya dapat mendaftar dengan telepon, laptop maupun alat pendukung permainan lainnya. Berikutnya, Anda menempatkan taruhan atau permainan yang akan Anda mainkan yang berada pada situs slot online. Dalam game slot online tersebut Anda dapat meraih kemenangan hadiahnya setiap saat serta tidak ada batasan waktu maupun umum sebab siapa saja dapat memainkan game slot online.</p>

                <p>Pernahkan Anda bermain judi online? bermain judi biasanya dapat dikerjakan secara langsung, namun saat ini dapat Anda akses dengan memanfaatkan teknologi yang semakin maju serta berkembang dengan cepat. Dengan banyaknya operator casino memakai situs maupun aplikasi mobile untuk menawarkan permainan taruhan pada pemain. Anda juga tidak perlu khawatir dalam keamanan judi online sebab sama halnya dengan casino offline, dimana ditata oleh komisi perjudian serta bekerja dengan kebijaksanaan fair play and responsible gaming yang ketat. Para pemain judi online umumnya bermain dengan mengirim dana pada situs judi online yang nantinya di isi ke account pemain hingga saat bertaruh dalam game judi, pemain menggunakan dana tersebut untuk bermain. Situs judi online pada umumnya memiliki feature penambahan serta beberapa animasi, membuat lebih menyenangkan untuk para pemain judi dengan sedikit ada batasan dalam bermain. Seperti slot online yang meliputi beberapa gulungan hingga 100 payline terpisah, sebab gameplay tidak dibatasi oleh ukuran mesin. Oleh karena itu, pemain bisa memastikan tipe role play dan line winning.</p>

                <p>Para pemain judi slot online tentu mengetahui situs STARS77 yang merupakan salah satu situs judi online yang memiliki fasilitas terlengkap dan terpercaya yang sudah memiliki lisensi resmi dari berbagai badan berwenang untuk menyelenggarakan kegiatan judi online. STARS77 juga menyediakan berbagai kategori permainan seperti Sports, Live Casino, Slot, Tembak Ikan, Lottery, Sabung Ayam dan kategori games. Bagaimana, menarik bukan? STARS77 sendiri menjadi pusat perhatian para slot online dengan menyajikan tingkat kemenangan atau winrate tertinggi yaitu 97%, sehingga mudah untuk Anda memenangkan jackpot. Selain itu STARS77 juga memiliki proses pendaftaran yang mudah serta singkat yang dilengkapi lebih dari 500+ permainan yang ditawarkan dengan 1 User Id atau modal Rp.20.000,- Anda sudah bisa menikmati semua permainan judi online.</p>


            @else
                <h1><strong>Situs Judi Slot Online Terlengkap &amp; Judi Online Terpercaya No.1 Di Indonesia</strong></h1>

                <p>Saat ini banyak judi online yang tersebar di Indonesia, atau yang sudah akrab sebagai salah satu metode cari uang tambahan. Dalam permainan game slot online, Anda akan memiliki berbagai macam permainan yang menarik serta memberikan keuntungan, jika Anda serius dalam memainkan game slot online tersebut. Apalagi ditambah dengan perubahan teknologi yang semakin maju, juga banyak perusahaan game slot online beradu untuk memberikan beberapa perjudian online dengan uang asli. Dengan didukung jaringan maupun akses internet yang lancar, Anda tentunya dapat mendaftar dengan telepon, laptop maupun alat pendukung permainan lainnya. Berikutnya, Anda menempatkan taruhan atau permainan yang akan Anda mainkan yang berada pada situs slot online. Dalam game slot online tersebut Anda dapat meraih kemenangan hadiahnya setiap saat serta tidak ada batasan waktu maupun umum sebab siapa saja dapat memainkan game slot online.&nbsp;</p>

                <p>Pernahkan Anda bermain judi online? bermain judi biasanya dapat dikerjakan secara langsung, namun saat ini dapat Anda akses dengan memanfaatkan teknologi yang semakin maju serta berkembang dengan cepat. Dengan banyaknya operator casino memakai situs maupun aplikasi mobile untuk menawarkan permainan taruhan pada pemain. Anda juga tidak perlu khawatir dalam keamanan judi online sebab sama halnya dengan casino offline, dimana ditata oleh komisi perjudian serta bekerja dengan kebijaksanaan fair play and responsible gaming yang ketat. Para pemain judi online umumnya bermain dengan mengirim dana pada situs judi online yang nantinya di isi ke account pemain hingga saat bertaruh dalam game judi, pemain menggunakan dana tersebut untuk bermain. Situs judi online pada umumnya memiliki feature penambahan serta beberapa animasi, membuat lebih menyenangkan untuk para pemain judi dengan sedikit ada batasan dalam bermain. Seperti slot online yang meliputi beberapa gulungan hingga 100 payline terpisah, sebab gameplay tidak dibatasi oleh ukuran mesin. Oleh karena itu, pemain bisa memastikan tipe role play dan line winning.</p>

                <p>Perlu Anda ketahui, tidak sedikit para pemain judi online yang baru saja terjun bermain game slot online dirugikan oleh pihak yang tidak bertanggung jawab, akibat salah memilih situs slot online. Oleh karena itu, selain memahami cara bermain, Anda juga harus mengetahui cara memilih situs slot online terpercaya yang merupakan hal mutlak yang perlu Anda ketahui dalam pencarian situs judi terpercaya. Untuk memastikan situs slot online terpercaya, Anda dapat mengetahui kebaikan dari situs tersebut dengan rekomendasi dari pemain yang sudah bermain di situs tersebut. Rekomendasi tersebut bisa Anda dapatkan dari teman yang sudah berpengalaman di bidang judi slot online atau rekomendasi lainnya bisa Anda dapatkan dengan bergabung terlebih dahulu di sebuah forum judi slot online, dimana di dalam forum tersebut Anda bisa menanyakan segala hal yang berkaitan dengan judi slot online, salah satunya menanyakan situs slot online terpercaya. Selain itu, Anda juga harus memastikan dari segala penawaran yang ditawarkan oleh situs judi slot online, memastikan ketentuan yang harus dilakukan para pemain jika akan mengambil penawaran yang diberikan. Tetapi, jika dirasa penawaran yang diberikan oleh pihak situs tersebut tidak masuk akal, maka kemungkinan besar situs tersebut palsu. Memang situs slot online terpercaya akan menawarkan berbagai macam bonus hingga promo yang menarik, tetapi penawaran tersebut tentu masih masuk akal serta tidak mengada-ngada.</p>
            @endif
        </div>
    </div>




    <ul>
        <li>&nbsp;</li>
    </ul>

</div>

<input type="hidden" id="particle-color" name="color" value="{{ $color }}">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/particles.js') }}"></script>
<script>
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
    let color = document.getElementById('particle-color').value;
    console.log(color)
    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value": 160,
                "density": {
                    "enable": true,
                    "value_area": 800
                }
            },
            "color": {
                "value": color
            },
            "shape": {
                "type": "triangle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                },
                "image": {
                    "src": "img/github.svg",
                    "width": 100,
                    "height": 100
                }
            },
            "opacity": {
                "value": 1,
                "random": true,
                "anim": {
                    "enable": true,
                    "speed": 1,
                    "opacity_min": 0,
                    "sync": false
                }
            },
            "size": {
                "value": 5,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 4,
                    "size_min": 0.3,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": false,
                "distance": 150,
                "color": "#ffffff",
                "opacity": 0.4,
                "width": 1
            },
            "move": {
                "enable": true,
                "speed": 1,
                "direction": "none",
                "random": true,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 600
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": true,
                    "mode": "bubble"
                },
                "onclick": {
                    "enable": true,
                    "mode": "repulse"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 400,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 250,
                    "size": 0,
                    "duration": 2,
                    "opacity": 0,
                    "speed": 3
                },
                "repulse": {
                    "distance": 400,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });
</script>
</body>
</html>
