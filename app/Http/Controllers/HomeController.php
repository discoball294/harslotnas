<?php

namespace App\Http\Controllers;

use App\Models\Leaderboard;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($brand = "stars77")
    {
        $leaderboard = Leaderboard::orderBy('turnover','desc')->limit(10)->get();
        $map_lb = $leaderboard->map(function ($item, $key){
            $replace_end = substr_replace($item->player_id, '***', -4);
            $item->player_id = substr_replace($replace_end, '**', 3, 1);
            $item->turnover = number_format($item->turnover);
            return $item;
        });

        if (in_array($brand, ["stars77", "77lucks", "luck99", "77dragon", "joker99"])) {
            $brand_details = [
                "stars77" => ["https://34.117.9.220/account/register", '#AD332A', "https://t.me/stars77info"],
                "77lucks" => ['https://34.107.163.137/account/register', '#291C4C', "https://t.me/lucks77info"],
                "luck99" => ["https://34.149.44.106/account/register", '#63D6F7', "https://t.me/luck99info"],
                "77dragon" => ["https://34.117.115.32/account/register", '#386809', "https://t.me/dragonoofc"],
                "joker99" => ["https://167.172.89.33/account/register", '#9158C5', "https://t.me/joker99info"]
            ];
            $url = $brand_details[$brand][0];
            $color = $brand_details[$brand][1];
            $telegram = $brand_details[$brand][2];



            return response()->view('home', compact('url', 'color', 'brand', 'telegram', 'leaderboard'));
        }
        return abort(404);
    }
}
